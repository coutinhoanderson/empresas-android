![N|Solid](logo_ioasys.png)

# README #

Estes documento README tem como objetivo fornecer as informações necessárias para execução e entendimento do projeto Empresas.

### Bibliotecas Utilizadas ###

* Retrofit: Para requests da api
* GSON: Para parse de Json e annotations
* Glide: Para manipular imagens
* Sweet-Alert-Dialog: Para dar feedback ao usuário
* Navigation: Para facilitar as transições de telas usando a Arquitetura Jetpack

### O que eu faria se tivesse mais tempo ###

* Faria o RecyclerView e Adapter com essa Lib https://github.com/CymChad/BaseRecyclerViewAdapterHelper
* Aplicaria o padrão MVVM
* Utilizaria injeção de dependência
* Aplicaria testes unitários
* Usaria RX, LiveData, DataBinding, Lifecycle, Room
* Aplicaria todos os conceitos do Jetpack
* Faria versão diferente pra tela em landscape
* Faria em Kotlin
* Aplicaria as animações desse conceito: https://github.com/agusibrahim/LoginPageConcept
* Documentaria o projeto


# Como executar #

### Casos de uso: ###
* Entre com o email: testeapple@ioasys.com.br e senha: 12341234 e clique em "ENTRAR"
* Clique no icone de busca para iniciar a busca
* Digite um nome de alguma empresa específica ou alguma letra para buscar os resultados -> EX: "Al"
* Clique no card da empresa para ver informações sobre ela
* Pressione a seta no canto superior esquerdo para retornar à tela anterior

### Casos de Teste ###

* Entrar com email inválido
* Entrar com email válido e senha inválida
* Entrar com ambos os campos vazios
* Entrar com um dos campos (Email ou Senha) vazios

