package br.com.coutinhoanderson.empresasandroid.model;

import com.google.gson.annotations.SerializedName;

class Portfolio {
    @SerializedName("enterprises_number")
    private String enterprisesNumber;
    private String[] enterprises;

    public String getEnterprisesNumber() {
        return enterprisesNumber;
    }

    public void setEnterprisesNumber(String enterprisesNumber) {
        this.enterprisesNumber = enterprisesNumber;
    }

    public String[] getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(String[] enterprises) {
        this.enterprises = enterprises;
    }
}
