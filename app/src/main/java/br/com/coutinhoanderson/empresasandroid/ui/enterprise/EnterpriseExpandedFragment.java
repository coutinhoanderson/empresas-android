package br.com.coutinhoanderson.empresasandroid.ui.enterprise;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

import br.com.coutinhoanderson.empresasandroid.R;
import br.com.coutinhoanderson.empresasandroid.model.Enterprise;

public class EnterpriseExpandedFragment extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.enterprise_expanded_fragment, container, false);
        ImageView enterpriseImg = view.findViewById(R.id.enterprise_image);
        TextView description = view.findViewById(R.id.enterprise_description);
        TextView title = view.findViewById(R.id.enterprise_name);
        view.findViewById(R.id.arrow_back_btn).setOnClickListener(v -> {
                    if (getActivity() != null)
                        getActivity().onBackPressed();
                }
        );
        if (getArguments() != null) {
            Enterprise enterprise = (Enterprise) getArguments().getSerializable("enterprise");
            if (enterprise != null) {
                description.setText(enterprise.getDescription());
                title.setText(enterprise.getEnterpriseName());
                if (enterprise.getPhoto() != null) {
                    Glide.with(this)
                            .load("http://empresas.ioasys.com.br/" + enterprise.getPhoto())
                            .into(enterpriseImg);
                } else {
                    Glide.with(this)
                            .load("https://www.cqse.eu/images/company/company-landscape.png")
                            .into(enterpriseImg);
                }
            }
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
