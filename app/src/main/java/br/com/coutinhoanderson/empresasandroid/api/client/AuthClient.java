package br.com.coutinhoanderson.empresasandroid.api.client;

import br.com.coutinhoanderson.empresasandroid.api.interfaces.IAuthService;

public class AuthClient extends ApiClient{
    public IAuthService authenticateUser(){
        return retrofit.create(IAuthService.class);
    }
}
