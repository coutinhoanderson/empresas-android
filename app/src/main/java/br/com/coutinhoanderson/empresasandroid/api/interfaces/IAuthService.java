package br.com.coutinhoanderson.empresasandroid.api.interfaces;

import br.com.coutinhoanderson.empresasandroid.model.ApiResponse;
import br.com.coutinhoanderson.empresasandroid.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface IAuthService {
    @POST("users/auth/sign_in")
    Call<ApiResponse> getApiData(@Body User user);
}
