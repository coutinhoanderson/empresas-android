package br.com.coutinhoanderson.empresasandroid.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import br.com.coutinhoanderson.empresasandroid.R;
import br.com.coutinhoanderson.empresasandroid.model.Headers;
import br.com.coutinhoanderson.empresasandroid.repository.DataRepository;

public class HomeFragment extends Fragment {
    private DataRepository mDataRepository;
    private Headers headers;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        mDataRepository = new DataRepository(this);
        if (getArguments() != null) {
            headers = (Headers) getArguments().getSerializable("headers");
        }
        RecyclerView mRecyclerView = view.findViewById(R.id.enterprise_list);
        SearchView searchView = view.findViewById(R.id.search_view);
        searchView.setOnSearchClickListener(v ->
                view.findViewById(R.id.logo_ioasys).setVisibility(View.INVISIBLE));
        searchView.setOnCloseListener(() -> {
            view.findViewById(R.id.logo_ioasys).setVisibility(View.VISIBLE);
            return false;
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                view.findViewById(R.id.start_hint).setVisibility(View.INVISIBLE);
                mDataRepository.getEnterprisesByName(query, headers, mRecyclerView);
                searchView.onActionViewCollapsed();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
