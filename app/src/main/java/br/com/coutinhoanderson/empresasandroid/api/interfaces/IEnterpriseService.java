package br.com.coutinhoanderson.empresasandroid.api.interfaces;

import br.com.coutinhoanderson.empresasandroid.model.EnterpriseResponse;
import br.com.coutinhoanderson.empresasandroid.model.Enterprises;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface IEnterpriseService {
    @Headers("Content-Type: application/json")
    @GET("enterprises")
    Call<Enterprises> getApiDataByName(
            @Query("name") String name, @Header("client") String client,
            @Header("access-token") String accessToken,
            @Header("uid") String uid);

    @Headers("Content-Type: application/json")
    @GET("enterprises/{id}")
    Call<EnterpriseResponse> getApiDataById(
            @Path("id") long id, @Header("client") String client,
            @Header("access-token") String accessToken,
            @Header("uid") String uid);
}
