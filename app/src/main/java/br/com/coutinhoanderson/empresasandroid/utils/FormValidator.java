package br.com.coutinhoanderson.empresasandroid.utils;

import android.text.TextUtils;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class FormValidator {
    public static boolean isRequiredEmpty(TextInputEditText textField) {
        boolean erro = false;
        TextInputLayout textInputLayout = (TextInputLayout) textField.getParent().getParent();
        if (TextUtils.isEmpty(textField.getText())) {
            textInputLayout.setError("Campo Obrigatório");
            erro = true;
        } else {
            textInputLayout.setErrorEnabled(false);
        }
        return erro;
    }
}
