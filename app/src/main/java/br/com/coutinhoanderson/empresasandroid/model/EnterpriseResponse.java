package br.com.coutinhoanderson.empresasandroid.model;

public class EnterpriseResponse {
    private Enterprise enterprise;
    private boolean success;

    public Enterprise getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(Enterprise enterprise) {
        this.enterprise = enterprise;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
