package br.com.coutinhoanderson.empresasandroid.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.coutinhoanderson.empresasandroid.R;
import br.com.coutinhoanderson.empresasandroid.model.Enterprise;

public class EnterpriseListAdapter extends RecyclerView.Adapter<EnterpriseListAdapter.ViewHolder> {
    private List<Enterprise> enterprises;
    private Fragment fragment;

    public EnterpriseListAdapter(List<Enterprise> enterprises, Fragment fragment) {
        this.enterprises = enterprises;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(fragment.getContext()).inflate(R.layout.card_enterprise, parent, false
                ));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Enterprise enterprise = enterprises.get(position);
        if (enterprise != null) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("enterprise", enterprise);
            holder.cardView.setOnClickListener(v -> NavHostFragment.findNavController(fragment).navigate(R.id.action_homeFragment_to_enterpriseExpandedFragment, bundle));
            holder.enterpriseName.setText(enterprise.getEnterpriseName());
            holder.enterpriseCountry.setText(enterprise.getCountry());
            if (enterprise.getEnterpriseType() != null)
                holder.enterpriseArea.setText(enterprise.getEnterpriseType().getEnterpriseTypeName());
            if (enterprise.getPhoto() != null) {
                Glide.with(fragment)
                        .load("http://empresas.ioasys.com.br/" + enterprise.getPhoto())
                        .into(holder.enterpriseImage);
            } else {
                Glide.with(fragment)
                        .load("https://www.cqse.eu/images/company/company-landscape.png")
                        .into(holder.enterpriseImage);
            }
        }

    }

    @Override
    public int getItemCount() {
        return enterprises.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView enterpriseName;
        private TextView enterpriseArea;
        private TextView enterpriseCountry;
        private ImageView enterpriseImage;
        private CardView cardView;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.enterprise_item);
            enterpriseName = itemView.findViewById(R.id.enterprise_name);
            enterpriseArea = itemView.findViewById(R.id.enterprise_area);
            enterpriseCountry = itemView.findViewById(R.id.enterprise_country);
            enterpriseImage = itemView.findViewById(R.id.enterprise_image);
        }
    }
}
