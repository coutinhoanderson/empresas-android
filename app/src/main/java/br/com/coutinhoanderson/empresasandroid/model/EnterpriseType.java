package br.com.coutinhoanderson.empresasandroid.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class EnterpriseType implements Serializable {
    private long id;
    @SerializedName("enterprise_type_name")
    private String enterpriseTypeName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEnterpriseTypeName() {
        return enterpriseTypeName;
    }

    public void setEnterpriseTypeName(String enterpriseTypeName) {
        this.enterpriseTypeName = enterpriseTypeName;
    }
}
