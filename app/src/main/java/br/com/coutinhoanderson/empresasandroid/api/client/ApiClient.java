package br.com.coutinhoanderson.empresasandroid.api.client;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class ApiClient {
    private static final String BASE_URL = "http://empresas.ioasys.com.br/api/v1/";
    Retrofit retrofit;

    ApiClient() {
        this.retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }


}
