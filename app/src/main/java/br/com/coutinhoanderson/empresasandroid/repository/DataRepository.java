package br.com.coutinhoanderson.empresasandroid.repository;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import java.util.Objects;

import br.com.coutinhoanderson.empresasandroid.R;
import br.com.coutinhoanderson.empresasandroid.api.client.AuthClient;
import br.com.coutinhoanderson.empresasandroid.api.client.EnterpriseClient;
import br.com.coutinhoanderson.empresasandroid.model.ApiResponse;
import br.com.coutinhoanderson.empresasandroid.model.EnterpriseResponse;
import br.com.coutinhoanderson.empresasandroid.model.Enterprises;
import br.com.coutinhoanderson.empresasandroid.model.Headers;
import br.com.coutinhoanderson.empresasandroid.model.User;
import br.com.coutinhoanderson.empresasandroid.ui.home.EnterpriseListAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataRepository {
    private Fragment fragment;
    private SweetAlertDialog errorDialog;
    private SweetAlertDialog pDialog;

    public DataRepository(Fragment fragment) {
        this.fragment = fragment;
        this.pDialog = new SweetAlertDialog(Objects.requireNonNull(fragment.getContext()), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(fragment.getString(R.string.loading_message)).setCancelable(false);
        this.errorDialog = new SweetAlertDialog(
                Objects.requireNonNull(fragment.getContext()), SweetAlertDialog.ERROR_TYPE)
                .setTitleText(fragment.getString(R.string.error_title))
                .setContentText(fragment.getString(R.string.error_message));
    }

    public void sendLoginRequest(User user) {
        Call<ApiResponse> call;
        pDialog.show();
        call = new AuthClient().authenticateUser().getApiData(user);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                pDialog.dismiss();
                if (response.code() == 200) {
                    Headers headers = new Headers();
                    headers.setAccessToken(response.headers().get("access-token"));
                    headers.setClient(response.headers().get("client"));
                    headers.setUid(response.headers().get("uid"));
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("headers", headers);
                    NavHostFragment.findNavController(fragment).navigate(R.id.action_loginFragment_to_homeFragment, bundle);
                } else {
                    errorDialog.show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {
                pDialog.dismiss();
                errorDialog.show();
            }
        });
    }

    public void getEnterprisesByName(String name, Headers headers, RecyclerView recyclerView) {
        Call<Enterprises> call;
        call = new EnterpriseClient().getEnterprises().getApiDataByName(
                name, headers.getClient(), headers.getAccessToken(), headers.getUid()
        );
        call.enqueue(new Callback<Enterprises>() {
            @Override
            public void onResponse(@NonNull Call<Enterprises> call, @NonNull Response<Enterprises> response) {
                if (response.code() == 200) {
                    Enterprises enterprises = response.body();
                    if (enterprises != null) {
                        recyclerView.setAdapter(
                                new EnterpriseListAdapter(enterprises.getEnterprises(), fragment)
                        );
                    }
                } else {
                    errorDialog.show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Enterprises> call, @NonNull Throwable t) {
                errorDialog.show();
            }
        });
    }

}
