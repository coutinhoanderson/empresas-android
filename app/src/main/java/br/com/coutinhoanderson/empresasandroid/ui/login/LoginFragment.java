package br.com.coutinhoanderson.empresasandroid.ui.login;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputEditText;

import br.com.coutinhoanderson.empresasandroid.R;
import br.com.coutinhoanderson.empresasandroid.model.User;
import br.com.coutinhoanderson.empresasandroid.repository.DataRepository;
import br.com.coutinhoanderson.empresasandroid.utils.FormValidator;

public class LoginFragment extends Fragment {
    private TextInputEditText emailInput;
    private TextInputEditText passwordInput;
    private boolean hasEmptyFields = false;
    private DataRepository mDataRepository;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_fragment, container, false);
        view.setAlpha(0);
        view.animate().setStartDelay(800).setDuration(2500).alpha(1).start();
        mDataRepository = new DataRepository(this);
        passwordInput = view.findViewById(R.id.input_password_edit);
        emailInput = view.findViewById(R.id.input_email_edit);
        view.findViewById(R.id.login_button).setOnClickListener(v -> {
                    if (emailInput.getText() != null && passwordInput.getText() != null) {
                        hasEmptyFields = (FormValidator.isRequiredEmpty(emailInput)
                                || FormValidator.isRequiredEmpty(passwordInput));
                        if (!hasEmptyFields)
                            mDataRepository
                                    .sendLoginRequest(new User(
                                            emailInput.getText().toString(),
                                            passwordInput.getText().toString())
                                    );
                    }
                }
        );
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
