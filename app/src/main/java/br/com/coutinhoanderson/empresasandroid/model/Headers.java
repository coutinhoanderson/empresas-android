package br.com.coutinhoanderson.empresasandroid.model;

import java.io.Serializable;

public class Headers implements Serializable {
    private String accessToken;
    private String uid;
    private String client;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }
}
