package br.com.coutinhoanderson.empresasandroid.model;

import com.google.gson.annotations.SerializedName;

class Investor {
    private long id;
    @SerializedName("investor_name")
    private String investorName;
    private String email;
    private String city;
    private String country;
    private long balance;
    private String photo;
    private Portfolio portfolio;
    @SerializedName("portfolio_value")
    private long portfolioValue;
    @SerializedName("first_access")
    private boolean firstAccess;
    @SerializedName("super_angel")
    private boolean superAngel;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getInvestorName() {
        return investorName;
    }

    public void setInvestorName(String investorName) {
        this.investorName = investorName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Portfolio getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(Portfolio portfolio) {
        this.portfolio = portfolio;
    }

    public long getPortfolioValue() {
        return portfolioValue;
    }

    public void setPortfolioValue(long portfolioValue) {
        this.portfolioValue = portfolioValue;
    }

    public boolean isFirstAccess() {
        return firstAccess;
    }

    public void setFirstAccess(boolean firstAccess) {
        this.firstAccess = firstAccess;
    }

    public boolean isSuperAngel() {
        return superAngel;
    }

    public void setSuperAngel(boolean superAngel) {
        this.superAngel = superAngel;
    }
}
