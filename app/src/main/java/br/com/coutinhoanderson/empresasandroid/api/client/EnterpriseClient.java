package br.com.coutinhoanderson.empresasandroid.api.client;

import br.com.coutinhoanderson.empresasandroid.api.interfaces.IEnterpriseService;

public class EnterpriseClient extends ApiClient {
    public IEnterpriseService getEnterprises() {
        return retrofit.create(IEnterpriseService.class);
    }
}