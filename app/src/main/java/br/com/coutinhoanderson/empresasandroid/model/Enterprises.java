package br.com.coutinhoanderson.empresasandroid.model;

import java.util.List;

public class Enterprises {
    private List<Enterprise> enterprises;

    public List<Enterprise> getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(List<Enterprise> enterprises) {
        this.enterprises = enterprises;
    }
}
